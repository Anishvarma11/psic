/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.utils;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mspace-dev
 */
public class Util {

    public static void printError(Exception ex) {
        try {
            Logger LOG = Logger.getLogger(Util.class.getName());
            LOG.log(Level.SEVERE, null, ex);
        } catch (Exception ex1) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex1);
        }
    }
    
}
