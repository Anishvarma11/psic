/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author mspace-dev
 */
public class Database {
    public static Connection getConnection(){   
        Connection conn=null;     
        try {
            String url = "jdbc:sqlite:mydb";
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection(url);
        } catch (ClassNotFoundException | SQLException e) {
            Util.printError(e);
        }
        return conn;
    }
}
