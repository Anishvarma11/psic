/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.dao;

import com.example.models.Doctor;
import com.example.utils.Database;
import com.example.utils.Util;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author mspace-dev
 */
public class DoctorDAO {

    private static final Connection CON = Database.getConnection();
    
    public static List<Doctor>  get(){        
        List<Doctor> doctors = new ArrayList<>();
        try {
            PreparedStatement ps = CON.prepareStatement("SELECT * FROM doctor");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Doctor doctor = new Doctor(
                        rs.getString("id"), rs.getString("fullName"), 
                        rs.getString("address"), rs.getString("phone")
                );
                doctors.add(doctor);
            }
        } catch (SQLException e) {
            Util.printError(e);
        }
        return doctors;
    }
    
    public static Doctor  get(int id){
        Doctor doctor = null;
        try {
            PreparedStatement ps = CON.prepareStatement("SELECT * FROM doctor WHERE id = ?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                doctor = new Doctor(
                        rs.getString("id"), rs.getString("fullName"), 
                        rs.getString("address"), rs.getString("phone")
                );
            }
        } catch (SQLException e) {
            Util.printError(e);
        }
        return doctor;        
    }
    
    public static boolean  add(Doctor data){
        boolean success = false;
        try {
            PreparedStatement ps = CON.prepareStatement("INSERT INTO doctor (fullname, address, phone) VALUES (?,?,?)");
            ps.setString(1, data.getFullName());
            ps.setString(2, data.getAddress());
            ps.setString(3, data.getPhone());
            ps.executeUpdate();
            success = true;
        } catch (SQLException e) {
            Util.printError(e);
        } 
        return success;
        
    }
}
