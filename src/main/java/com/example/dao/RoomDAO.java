/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.dao;

import com.example.models.Room;
import com.example.utils.Database;
import com.example.utils.Util;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author mspace-dev
 */
public class RoomDAO {

    private static final Connection CON = Database.getConnection();
    
    public static List<Room>  get(){        
        List<Room> rooms = new ArrayList<>();
        try {
            PreparedStatement ps = CON.prepareStatement("SELECT * FROM room");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Room room = new Room(
                        rs.getString("id"), rs.getString("name"), 
                        rs.getString("description")
                );
                rooms.add(room);
            }
        } catch (SQLException e) {
            Util.printError(e);
        }
        return rooms;
    }
    
    public static Room  get(int id){
        Room room = null;
        try {
            PreparedStatement ps = CON.prepareStatement("SELECT * FROM room WHERE id = ?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                room = new Room(
                        rs.getString("id"), rs.getString("name"), 
                        rs.getString("description")
                );
            }
        } catch (SQLException e) {
            Util.printError(e);
        }
        return room;        
    }
    
    public static boolean  add(Room data){
        boolean success = false;
        try {
            PreparedStatement ps = CON.prepareStatement("INSERT INTO room (name, description) VALUES (?,?)");
            ps.setString(1, data.getName());
            ps.setString(2, data.getDescription());
            ps.executeUpdate();
            success = true;
        } catch (SQLException e) {
            Util.printError(e);
        } 
        return success;
        
    }
}
