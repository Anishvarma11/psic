/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.dao;

import com.example.models.User;
import com.example.utils.Database;
import com.example.utils.Util;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author mspace-dev
 */
public class LoginDAO {

    private static final Connection CON = Database.getConnection();

    public static User login(String userId) {
        User user = null;
        try {
            PreparedStatement ps = CON.prepareStatement("SELECT * FROM user WHERE id = ?");
            ps.setString(1, userId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                user = new User(
                        rs.getString("id"), rs.getString("fullName"), 
                        rs.getString("address"), rs.getString("phone"), 
                        rs.getBoolean("isAdmin")
                );
            }

            if (user != null) {
                ps = CON.prepareStatement("DELETE FROM user_session");
                ps.executeUpdate();

                ps = CON.prepareStatement("INSERT INTO user_session (userId) VALUES (?)");
                ps.setString(1, userId);
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            Util.printError(e);
        } finally {
            try {
                CON.close();
            } catch (SQLException e) {
                Util.printError(e);
            }
        }
        return user;
    }
}
