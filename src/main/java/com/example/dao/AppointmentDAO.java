/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.dao;

import com.example.models.Appointment;
import com.example.utils.Database;
import com.example.utils.Util;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author mspace-dev
 */
public class AppointmentDAO {

    private static final Connection CON = Database.getConnection();
    
    public static List<Appointment>  get(){        
        List<Appointment> doctors = new ArrayList<>();
        try {
            PreparedStatement ps = CON.prepareStatement("SELECT * FROM appointment");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Appointment appointment = new Appointment(
                        rs.getString("id"), rs.getString("patientId"), 
                        rs.getString("patientName"),rs.getString("doctor"),
                        rs.getString("treatment"), rs.getString("room"), 
                        rs.getString("date")
                );
                doctors.add(appointment);
            }
        } catch (SQLException e) {
            Util.printError(e);
        }
        return doctors;
    }
    
    public static Appointment  get(int id){
        Appointment appointment = null;
        try {
            PreparedStatement ps = CON.prepareStatement("SELECT * FROM appointment WHERE id = ?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                appointment = new Appointment(
                        rs.getString("id"), rs.getString("patientId"), 
                        rs.getString("patientName"),rs.getString("doctor"),
                        rs.getString("treatment"), rs.getString("room"), 
                        rs.getString("date")
                );
            }
        } catch (SQLException e) {
            Util.printError(e);
        }
        return appointment;        
    }
    
    public static boolean  add(Appointment data){
        boolean success = false;
        try {
            PreparedStatement ps = CON.prepareStatement("INSERT INTO appointment (patientId,  patientName,  doctor,  treatment,  room,  date) VALUES (?,?,?,?,?,?)");
            ps.setString(1, data.getPatientId());
            ps.setString(2, data.getPatientName());
            ps.setString(3, data.getDoctor());
            ps.setString(4, data.getTreatment());
            ps.setString(5, data.getRoom());
            ps.setString(6, data.getDate());
            ps.executeUpdate();
            success = true;
        } catch (SQLException e) {
            Util.printError(e);
        } 
        return success;
        
    }
}
