/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.dao;

import com.example.models.User;
import com.example.utils.Database;
import com.example.utils.Util;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author mspace-dev
 */
public class UserDAO {

    private static final Connection CON = Database.getConnection();

    public static List<User> get() {
        List<User> users = new ArrayList<>();
        try {
            PreparedStatement ps = CON.prepareStatement("SELECT * FROM user WHERE isAdmin != 1");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                User user = new User(
                        rs.getString("id"), rs.getString("fullName"),
                        rs.getString("address"), rs.getString("phone"),
                        rs.getBoolean("isAdmin")
                );
                users.add(user);
            }
        } catch (SQLException e) {
            Util.printError(e);
        }
        return users;
    }

    public static User get(int id) {
        User user = null;
        try {
            PreparedStatement ps = CON.prepareStatement("SELECT * FROM user WHERE id = ?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                user = new User(
                        rs.getString("id"), rs.getString("fullName"),
                        rs.getString("address"), rs.getString("phone"),
                        rs.getBoolean("isAdmin")
                );
            }
        } catch (SQLException e) {
            Util.printError(e);
        }
        return user;
    }

    public static boolean add(User data) {
        boolean success = false;
        try {
            PreparedStatement ps = CON.prepareStatement("INSERT INTO user (fullname, address, phone, isAdmin) VALUES (?,?,?,?)");
            ps.setString(1, data.getFullName());
            ps.setString(2, data.getAddress());
            ps.setString(3, data.getPhone());
            ps.setBoolean(4, data.isAdmin());
            ps.executeUpdate();
            success = true;
        } catch (SQLException e) {
            Util.printError(e);
        }
        return success;

    }
}
