/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.dao;

import com.example.models.Treatment;
import com.example.utils.Database;
import com.example.utils.Util;
import java.util.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author mspace-dev
 */
public class TreatmentDAO {

    private static final Connection CON = Database.getConnection();
    
    public static List<Treatment>  get(){        
        List<Treatment> treatments = new ArrayList<>();
        try {
            PreparedStatement ps = CON.prepareStatement("SELECT * FROM treatment");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Treatment treatment = new Treatment(
                        rs.getString("id"), rs.getString("doctor"), 
                        rs.getString("treatment"), rs.getString("room"), 
                        rs.getString("date")
                );
                treatments.add(treatment);
            }
        } catch (SQLException e) {
            Util.printError(e);
        }
        return treatments;
    }
    
    public static Treatment  get(int id){
        Treatment treatment = null;
        try {
            PreparedStatement ps = CON.prepareStatement("SELECT * FROM treatment WHERE id = ?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                treatment = new Treatment(
                        rs.getString("id"), rs.getString("doctor"), 
                        rs.getString("treatment"), rs.getString("room"), 
                        rs.getString("date")
                );
            }
        } catch (SQLException e) {
            Util.printError(e);
        }
        return treatment;        
    }
    
    public static boolean  add(Treatment data){
        boolean success = false;
        try {
            PreparedStatement ps = CON.prepareStatement("INSERT INTO treatment (doctor, treatment, room, date) VALUES (?,?,?,?)");
            ps.setString(1, data.getDoctor());
            ps.setString(2, data.getTreatment());
            ps.setString(3, data.getRoom());
            ps.setString(4, data.getDate());
            ps.executeUpdate();
            success = true;
        } catch (SQLException e) {
            Util.printError(e);
        } 
        return success;
        
    }
}
