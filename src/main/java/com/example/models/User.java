/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.models;

/**
 *
 * @author mspace-dev
 */
public class User {
    private String id;
    private String fullName;
    private String address;
    private String phone;
    private boolean admin;

    public User(String id, String fullName, String address, String phone, boolean admin) {
        this.id = id;
        this.fullName = fullName;
        this.address = address;
        this.phone = phone;
        this.admin = admin;
    }

    public User(String fullName, String address, String phone, boolean admin) {
        this.fullName = fullName;
        this.address = address;
        this.phone = phone;
        this.admin = admin;
    }

    public User() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    
}
