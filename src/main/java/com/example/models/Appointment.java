/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.models;

/**
 *
 * @author mspace-dev
 */
public class Appointment {
    private String id;
    private String patientId;
    private String patientName;
    private String doctor;
    private String treatment;
    private String room;
    private String date;

    public Appointment(String id, String patientId, String patientName, String doctor, String treatment, String room, String date) {
        this.id = id;
        this.patientId = patientId;
        this.patientName = patientName;
        this.doctor = doctor;
        this.treatment = treatment;
        this.room = room;
        this.date = date;
    }

    public Appointment(String patientId, String patientName, String doctor, String treatment, String room, String date) {
        this.patientId = patientId;
        this.patientName = patientName;
        this.doctor = doctor;
        this.treatment = treatment;
        this.room = room;
        this.date = date;
    }

    public Appointment(String patientName, String doctor, String treatment, String room, String date) {
        this.patientName = patientName;
        this.doctor = doctor;
        this.treatment = treatment;
        this.room = room;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public String getTreatment() {
        return treatment;
    }

    public void setTreatment(String treatment) {
        this.treatment = treatment;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    
    
}
