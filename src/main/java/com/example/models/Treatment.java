/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.models;

/**
 *
 * @author mspace-dev
 */
public class Treatment {
    private String id;
    private String doctor;
    private String treatment;
    private String room;
    private String date;

    public Treatment(String id, String doctor, String treatment, String room, String date) {
        this.id = id;
        this.doctor = doctor;
        this.treatment = treatment;
        this.room = room;
        this.date = date;
    }

    public Treatment(String doctor, String treatment, String room, String date) {
        this.doctor = doctor;
        this.treatment = treatment;
        this.room = room;
        this.date = date;
    }

    public Treatment() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public String getTreatment() {
        return treatment;
    }

    public void setTreatment(String treatment) {
        this.treatment = treatment;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    
    
    
}
